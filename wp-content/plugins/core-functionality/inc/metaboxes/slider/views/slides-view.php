<?php
$title = esc_html( $slide['crb_slide_title'] );
$image = wp_get_attachment_image( $slide['crb_slide_image'], 'full' );
$text  = $slide['crb_slide_text'];
$url   = $slide['crb_slide_url'];
?>
	<li>
		<div class="uk-panel uk-panel-box">
			<div class="uk-grid uk-grid-width-large-1-2">
				<div class="tm-slideshow-img   uk-align-center "
				     >
					<a href="<?php echo $url?>" target="_blank">
						<?php echo $image; ?>
					</a>
				</div>
				<div class="tm-slideshow-text" >
					<a href="<?php echo $url?>" target="_blank">
						<h3 class="uk-panel-title uk-text-center"><?php echo $title; ?></h3>
					</a>

					<p><?php echo $text; ?></p>
					<a href="<?php echo $url; ?>"
					   target="_blank"
					   class="uk-button
									uk-button-primary">View
						Site</a>
				</div>
			</div>

		</div>
	</li>

