<div class=" tm-testimonial-slideshow uk-width-medium-7-10 uk-container-center uk-margin-large uk-slidenav-position"
     data-uk-slideshow="{autoplay:true,
		animation:'scroll', autoplay:true, height:450px}">
	<ul class="uk-slideshow">
		<?php wst_get_simple_slides('crb_testimonial_slides','views/testimonial-slides-view.php');?>

	</ul>
<!--	<a href=""-->
<!--	   class="uk-slidenav  uk-slidenav-previous"-->
<!--	   data-uk-slideshow-item="previous"></a>-->
<!--	<a href=""-->
<!--	   class="uk-slidenav  uk-slidenav-next"-->
<!--	   data-uk-slideshow-item="next"></a>-->
	<ul class="uk-dotnav uk-visible-large uk-dotnav-contrast uk-position-bottom uk-flex-center">
		<?php wst_get_simple_dotnav_items('crb_testimonial_slides');?>
	</ul>
</div>