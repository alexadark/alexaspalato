<?php

$author_image = wp_get_attachment_image( $slide['crb_author_image'], 'full' );
$text  = $slide['crb_testimonial_text'];
$author   = $slide['crb_author'];
?>
<li>
	<div class="uk-panel uk-text-center">
		<div class="testimonial-author-img uk-border-circle">
		<?php echo $author_image; ?>
		</div>
		<p class="testimonial-text"><span>"</span><?php echo $text;?></p>
		<p class="testimonial-author">- <?php echo $author; ?></p>

	</div>
</li>

