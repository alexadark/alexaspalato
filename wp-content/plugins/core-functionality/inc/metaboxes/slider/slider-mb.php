<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make('post_meta','Portfolio Slider')
->show_on_post_type('page')
->show_on_page( (int) get_option( 'page_on_front' ) )
->add_fields(array(
Field::make('complex','crb_slides')->set_layout('tabbed')
->add_fields(array(
Field::make('image','crb_slide_image'),
Field::make('text','crb_slide_title'),
Field::make('textarea','crb_slide_text'),
Field::make('text','crb_slide_url')
)),

));

Container::make('post_meta','Testimonial Slider')
->show_on_post_type('page')
->show_on_page( (int) get_option( 'page_on_front' ) )
->add_fields(array(

Field::make('complex','crb_testimonial_slides')->set_layout('tabbed')
->add_fields(array(
Field::make('image','crb_author_image'),
Field::make('rich_text','crb_testimonial_text'),
Field::make('text','crb_author')
))
));
