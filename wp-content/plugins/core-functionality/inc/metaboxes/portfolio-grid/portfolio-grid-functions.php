<?php

function wst_get_complex_field_group($group_view_path){
	include($group_view_path);
}

function wst_get_items($item_field, $item_view_path){
	$items = carbon_get_the_post_meta($item_field,'complex');
	if(!$items){
		return;
	}
	foreach ( $items as $item ) {
		include(($item_view_path));

	}

}