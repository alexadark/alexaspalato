<?php
$title = esc_html( $item['crb_pf_title'] );
$image = wp_get_attachment_image( $item['crb_pf_image'], 'full' );
$text  = $item['crb_pf_text'];
$url   = $item['crb_pf_link'];
?>
<div class="portfolio-item">
	<div class="uk-panel ">
		<a href="<?php echo $url ?>"
		   target="_blank"><?php echo $image; ?></a>
		<a href="<?php echo $url ?>"
		   target="_blank">
			<h3 class="uk-panel-title uk-text-center"><?php echo $title; ?></h3>
		</a>
		<div class="tm-portfolio-item-text">
			<?php echo $text; ?>
			<a class="tm-pf-link" href="<?php echo $url ?>"
			   target="_blank">View Site</a>
		</div>

	</div>
</div>
