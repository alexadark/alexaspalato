<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;


Container::make( 'post_meta', 'Portfolio Grid' )
         ->show_on_post_type( 'page' )
         ->show_on_page( (int) get_option( 'page_on_front' ) )
         ->add_fields( array(

	         Field::make( 'complex', 'crb_portfolio_items' )->set_layout( 'tabbed' )
	              ->add_fields( array(
		              Field::make( 'image', 'crb_pf_image' ),
		              Field::make( 'text', 'crb_pf_title' ),
		              Field::make( 'textarea', 'crb_pf_text' )
		                   ->set_rows( $rows = 5 ),
		              Field::make( 'text', 'crb_pf_link' ),
	              ) )
         ) );
