<?php
beans_remove_markup( 'beans_fixed_wrap_main' );
beans_remove_attribute( 'beans_main', 'class', 'uk-block' );
beans_remove_attribute( 'beans_post', 'class', 'uk-panel-box' );


beans_add_smart_action( 'beans_post_prepend_markup', 'wst_display_about_section' );

function wst_display_about_section() {
	include ('views/about-section-view.php');
 }

beans_add_smart_action( 'beans_post_prepend_markup', 'wst_display_portfolio_section', 11 );
function wst_display_portfolio_section() {
	include 'views/portfolio-section-view.php';
}

beans_add_smart_action( 'beans_post_prepend_markup', 'wst_display_testimonial_section', 12 );
function wst_display_testimonial_section() {
	include 'views/testimonials-section.php';
}

beans_add_smart_action( 'beans_post_prepend_markup', 'wst_display_contact_section', 13 );
function wst_display_contact_section() {
	include 'views/contact-section.php';
}

beans_load_document();