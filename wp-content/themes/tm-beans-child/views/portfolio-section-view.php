<div class="top-angle portfolio"></div>
<div id="portfolio">
	<div class="portfolio-content  uk-width-large-2-3 uk-container-center">
		<h2 class="tm-section-title uk-text-center">Portfolio</h2>
		<?php beans_display_divider(); ?>
		<div class="tm-section-text-block uk-text-center">Some of my most recents works...
		</div>

	</div>

	<?php wst_get_simple_slider( 'views/slider-view.php' ); ?>

	<div class="uk-button uk-button-primary uk-align-center uk-width-9-10 uk-width-medium-1-3 uk-width-large-2-10"
	     data-uk-modal="{target:'#portfolio-content'}">View all works
	</div>
	<div id="portfolio-content"
	     class="uk-modal">
		<div class="tm-modal-portfolio uk-modal-dialog uk-modal-dialog-blank uk-animation-scale-up ">
			<a class="uk-modal-close uk-close uk-close-alt"></a>

			<div class="uk-container uk-container-center uk-margin-large">
				<?php wst_get_complex_field_group('views/portfolio-grid-view.php');?>
				<h1 class="uk-h1 uk-text-center">And Many More...</h1>
			</div>

		</div>
	</div>
</div>
<div class="bottom-angle portfolio"></div>
