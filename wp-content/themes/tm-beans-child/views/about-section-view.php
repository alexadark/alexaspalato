<div class="top-angle about"></div>
<section id="about"
         class="">

	<div class="about-content uk-width-large-2-3 uk-container-center">

		<h2 class="tm-section-title uk-text-center">About</h2>
		<?php beans_display_divider(); ?>
		<p class="tm-section-text-block uk-container uk-container-center ">
			I love code and i love design, and think that both must be equally beautifull!<br>
			My work is my passion and i am totally commited to it. <br>
			Being one of the happy few hand picked wordpress experts of codeable, has given me an international
			clients network, you can see my profile <a href="https://codeable.io/developers/alexandra-spalato/"
			                                           target="_blank"
			>here</a>, as well as my <a href="https://codeable.io/changing-lives-alexandra-spalato/"
			                            target="_blank">Interview</a> with them. <br>
			I am specialized in developing beautiful themes with the Genesis framework, and more recently i also
			fall in love with the new <a href=""
			                             target="_blank">beans framework</a> <br>
			I also have a team working with me, which allow me to complete the most ambitious projects in the
			best timing. <br>
			Of course beautifull code and design must be supported by a great communication, as well as
			responsiveness and respect of the deadlines. <br>
			These are my values
		</p>
		<a class="uk-button uk-button-primary uk-align-center uk-width-1-2 uk-width-medium-1-3 uk-width-large-2-10"
		   href="#services"
		   data-uk-modal="">Services</a>
		<div id="services"
		     class="uk-modal">
			<div class="uk-modal-dialog uk-modal-dialog-blank uk-height-viewport
			        uk-animation-slide-bottom uk-flex uk-flex-center uk-flex-middle">
				<a href=""
				   class="uk-modal-close  uk-close uk-close-alt "></a>
				<div class="uk-grid ">
					<div class="tm-service-item uk-width-medium-1-3">
						<div class="uk-panel">
							<div class="uk-panel-title">Service 1</div>
						</div>
						<div class="uk-panel-body"></div>
					</div>
					<div class="tm-service-item uk-width-medium-1-3">
						<div class="uk-panel">
							<div class="uk-panel-title">Service 1</div>
						</div>
						<div class="uk-panel-body"></div>
					</div>
					<div class="tm-service-item uk-width-medium-1-3">
						<div class="uk-panel">
							<div class="uk-panel-title">Service 1</div>
						</div>
						<div class="uk-panel-body"></div>
					</div>
				</div>
			</div>
		</div>

	</div>

</section>
<div class="bottom-angle about"></div>