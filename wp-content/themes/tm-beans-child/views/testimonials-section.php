<div class="top-angle testimonials"></div>
<section id="testimonials">
	<div class="testimonials-content uk-width-8-10 uk-width-large-2-3 uk-container-center">
		<h2 class="tm-section-title uk-text-center">Testimonials</h2>
		<?php beans_display_divider(); ?>
		<!--			<p class="tm-section-text-block uk-text-center">Read what other says about my work!</p>-->

	</div>
	<?php wst_get_simple_slider( 'views/testimonial-slider-view.php' ); ?>
</section>
<div class="bottom-angle testimonials"></div>