<div class="top-angle contact"></div>
<section id="contact">
	<div class="contact-content uk-width-8-10 uk-width-large-2-3 uk-container-center">
		<h2 class="tm-section-title uk-text-center">Contact</h2>
		<?php beans_display_divider(); ?>
		<div class="uk-grid uk-grid-width-large-1-2">
			<div class="uk-panel form-text ">
				A project, a website, a question ? <br>
				Drop me a line! <br>
				Or call me directly <br>
				tel: <a href="tel:0034604182735">+34 604 18 27 35</a><br>
				<script type="text/javascript" src="https://secure.skypeassets.com/i/scom/js/skype-uri.js"></script>
				<div id="SkypeButton_Call_crea-webdesign_1">
					<script type="text/javascript" src="https://secure.skypeassets.com/i/scom/js/skype-uri.js"></script>
					<div id="SkypeButton_Call_crea-webdesign_1">
						<script type="text/javascript">
							Skype.ui({
								"name": "chat",
								"element": "SkypeButton_Call_crea-webdesign_1",
								"participants": ["crea-webdesign"],
								"imageColor": "white",
								"imageSize": 32
							});
						</script>
					</div>

				</div>

			</div>
			<div class=" tm-contact-form">
				<?php gravity_form( 1, $display_title = false, $display_description = false, $display_inactive = false,
					$field_values = null, $ajax = true, $tabindex, $echo = true ); ?>
			</div>
		</div>

	</div>
</section>
<div class="bottom-angle contact"></div>