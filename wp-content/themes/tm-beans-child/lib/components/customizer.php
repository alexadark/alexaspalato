<?php
function wst_register_customizer($wp_customize){
	$wp_customize->add_section(
		'home_settings_section',
		array(
			'title' => 'Home Settings',
			'description' => 'This is the home section settings.',
			'priority' => 300,
		)
	);

	$wp_customize->add_setting(
		'author_image'
	);
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'author_image',
			array(
				'label' => 'Author Image',
				'section' => 'home_settings_section',
				'settings' => 'author_image'
			)
		)
	);

}
add_action('customize_register','wst_register_customizer');