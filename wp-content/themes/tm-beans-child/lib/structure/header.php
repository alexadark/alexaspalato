<?php
beans_add_smart_action( 'wp', 'wst_set_up_header_structure' );
function wst_set_up_header_structure() {
//	Center site branding
	beans_replace_attribute( 'beans_site_branding', 'class', 'uk-float-left', 'uk-text-center  ' );
	beans_add_attribute( 'beans_site_title_link', 'data-uk-animatedtext','' );
	beans_replace_attribute('beans_site_title_tag','class','uk-text-small uk-text-muted','
	uk-animation-scale-down uk-animation-2');

	

	beans_add_smart_action( 'beans_site_title_link_after_markup', 'beans_display_divider' );
	function beans_display_divider() {
		echo beans_open_markup( 'beans_header_divider', 'div', array( 'class' => 'divider uk-margin uk-text-center',
			'data-uk-scrollspy'=>'{cls:\'uk-animation-slide-bottom \',repeat:true}') );
			echo beans_open_markup('beans_divider_icon','div',array('class' => 'uk-icon-code uk-icon-small'));
			echo beans_close_markup( 'beans_divider_icon', 'div' );
		echo beans_close_markup( 'beans_header_divider', 'div' );
	}

	beans_add_smart_action( 'beans_header_after_markup', 'beans_display_header_author_image' );
	function beans_display_header_author_image() { ?>
		<div class="bottom-angle header"></div>
<?php
		echo beans_open_markup( 'beans_header_author_image', 'img', array(
			'class'  => 'author-image uk-border-circle uk-align-center uk-animation-scale-up uk-animation-1',
			'src'    => get_theme_mod('author_image'),
			'alt'    => 'author-image',
			'width'  => '150px',
			'height' => '150px',
		) );
	}

	beans_wrap_inner_markup('beans_header','beans_header_content','div', array('class'=>'header-content'));

	

	// Breadcrumb
	beans_remove_action( 'beans_breadcrumb' );

} ?>

