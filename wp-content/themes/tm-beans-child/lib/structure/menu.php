<?php



// Modify primary menu type to stack as it would in a sidenav.
add_filter( 'beans_primary_menu_args', 'wst_primary_menu_args' );

function wst_primary_menu_args( $args ) {

	return array_merge( $args, array(
		'beans_type' => 'sidenav'
	) );

}

beans_replace_attribute('beans_menu_sidenav_primary','class','uk-visible-large','uk-hidden-small');

// Add the modal menu button.
add_action( 'beans_site_branding_before_markup', 'wst_modal_menu_button' );

function wst_modal_menu_button() {
	?>
	<a class="uk-float-right uk-hidden-small" href="#tm-modal-menu" data-uk-modal>
		<?php wst_get_menu_icon(); ?>
	</a>
	<?php

}

// Add the modal menu with an action to which the menu will be attached.
add_action( 'beans_site_after_markup', 'wst_modal_menu' );

function wst_modal_menu() {

	?>
	<div id="tm-modal-menu" class="uk-modal">

		<div class="tm-modal-menu uk-modal-dialog uk-modal-dialog-blank uk-height-viewport uk-text-center
		uk-animation-slide-top">
			<a class="uk-modal-close uk-close uk-close-alt"></a>
			<?php do_action( 'wst_modal_menu' ); ?>
		</div>
	</div>

	<?php

}

// Change the primary menu hook to the new modal hook and remove floating class.
beans_modify_action_hook( 'beans_primary_menu', 'wst_modal_menu' );
beans_remove_attribute( 'beans_primary_menu', 'class', 'uk-float-right' );

// Move the primary menu offcanvas button back to the header.
beans_modify_action_hook( 'beans_primary_menu_offcanvas_button', 'beans_site_branding_after_markup' );

/*--------------------------------------------------------------
OFF CANVAS
--------------------------------------------------------------*/
beans_replace_attribute('beans_primary_menu_offcanvas_button','class','uk-button','uk-visible-small');
beans_modify_action_hook('beans_primary_menu_offcanvas_button','beans_site_branding_before_markup');
beans_remove_output('beans_offcanvas_menu_button');
