<?php
beans_add_smart_action('beans_footer_before_markup','wst_display_footer_angle');
function wst_display_footer_angle(){ ?>
 <div class="top-angle footer"></div>
<?php }

// 10.1 Overwrite the Footer content

beans_modify_action_callback( 'beans_footer_content', 'beans_child_footer_content' );

function beans_child_footer_content() {
 include ('views/footer-view.php');
}